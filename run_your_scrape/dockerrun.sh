SH=`cd $(dirname $BASH_SOURCE) && pwd`
#
SCRAPE_D=${SCRAPE_D:-"$SH/external_scraping_code/"}
OUTPUT_D=${OUTPUT_D:-"$SH/output.d/"}
#
c='scrape_that_web_dockercontainer'_c
docker rm -f $c || true
docker run \
    --name $c \
    \
    -v $SCRAPE_D:/app/'external_scraping_code/'  \
    -v $OUTPUT_D:/app/'output.d/'                \
    'scrape_that_web_dockercontainer'_i
