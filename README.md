As scraping a webpage w/ Selenium require infra/lib tiring setup

I make this Docker image so as we can 'plug in' selenium webdriver code to run as simple/fast as possilbe 

How to use
00 refer to `dockerrun.sh` in :gitcloned_d/run_your_scrape/
01 thru that script, below things are setup 
  a) ./external_scraping_code/`scrape.py` - WRITE YOUR SCRAPE here, refer a sample at scrape_sample.py 
  b) ./`output.d/` to store result - run `dockerrun.sh` will produce output here
