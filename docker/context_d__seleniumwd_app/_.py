"""
ref Bard chatbot [view this chat](https://g.co/bard/share/d68800eb6696),
    and as Bard can export to replit, I exported it to replit [here](https://replit.com/@NamG1/selenium-webdriver#)

pip package
python3 -m pipenv install  selenium webdriver_manager
"""
import os.path
#
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options


ChromeDriverManager().install()  # auto install any required chrome webdriver

'''option soas to run chrome webdriver properly/noerror'''
o = Options()
o.add_argument('--headless')  #NOTE we MUST use --headless mode then .set_window_size() to capture whole page height
o.add_argument("--disable-gpu")  #NOTE this is required wh/ run in container  ref. https://dev.to/googlecloud/using-headless-chrome-with-cloud-run-3fdp
o.add_argument("--no-sandbox")   #NOTE this is required wh/ run in container  ref. https://dev.to/googlecloud/using-headless-chrome-with-cloud-run-3fdp
wd = webdriver.Chrome(options=o)

WD_IMPLICITLY_WAIT=6
wd.implicitly_wait(WD_IMPLICITLY_WAIT)

#region call external selenium-scraping-code
try:
    from external_scraping_code.scrape import scrape
    scrape(wd)
except Exception as e:
    raise e
#endregion call external selenium-scraping-code

##region save output
THIS_FILE_D = os.path.dirname(__file__)
OUTPUT_D    = f'{THIS_FILE_D}/output.d'

#region save html source
with open(f'{OUTPUT_D}/pagesource.html', 'w') as f: f.write(wd.page_source)  # this only save html file ref. ref https://g.co/bard/share/d18c686bbc35
#endregion save html source

#region take snapshot
# set full width+height to capture the whole-height content
height = wd.execute_script('return document.documentElement.scrollHeight')
width  = wd.execute_script('return document.documentElement.scrollWidth')
wd.set_window_size(width=width + 100, height=height + 100)  # the trick here  # use +100 for buffer spaces

r = wd.save_screenshot(f'{OUTPUT_D}/yymmdd.png')
#                     (must use FULL PATH here)
if not r: raise Exception()  # for full codeflow, check above .png saving results True/succeeded
#endregion take snapshot

##endregion save output

wd.close()
