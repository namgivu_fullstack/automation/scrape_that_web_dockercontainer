SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`

echo "# this 1strun should fail as require 'extra-required dependency' "
. "$SH/_install_webdriver.sh" || true

#region
echo 'use ldd to install extra-required dependency'
f="/tmp/apt_install_extrarequireddep.sh"

chromedriver_binary=`find $HOME/.wdm/ -name chromedriver -type f`  # eg /root/.wdm/drivers/chromedriver/linux64/114.0.5735.90/chromedriver
ls -lah $chromedriver_binary

ldd $chromedriver_binary \
    |  grep 'not found'                                         \
    `# grep to filter those line eg libnss3.so => not found`    \
    `#                              libnspr4.so => not found`   \
    | cut -d'=' -f1 `#           eg libnss3.so libnspr4.so`     \
    | cut -d'.' -f1 `#           eg libnss3    libnspr4`        \
    | xargs -i echo 'echo;echo; apt-get install -y {}' > $f

echo
echo 'temp .sh script to run'
echo    $f
ls -lah $f
cat     $f
echo; cat $f; chmod +x $f; . $f 2>&1 | tee "$f.console.log"

echo; cat "$f.console.log" | grep 'E:'  # list error ifany
#endregion use ldd to install extra-required dependency

echo "# this 2ndrun should pass now as required 'extra-required dependency' been installed "
. "$SH/_install_webdriver.sh" || true
