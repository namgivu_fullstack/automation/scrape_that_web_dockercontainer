SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
AH=`cd $SH/.. && pwd`

cd $AH
    # run ChromeDriverManager().install() to install webdriver binary to default location  #NOTE will fail at 1strun --> we get :ldd to list the required dependency ref. TODO
    PIPENV_DONT_LOAD_ENV=1 \
    PYTHONPATH=$AH pipenv run \
        python -c '
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import os; os.environ["WDM_LOCAL"] = "0"  # default download to user.home/.wdm  ref. https://pypi.org/project/webdriver-manager/ @ WDM_LOCAL

ChromeDriverManager().install()

o = webdriver.ChromeOptions()
o.add_argument("--headless")
o.add_argument("--disable-gpu")
o.add_argument("--no-sandbox")
wd = webdriver.Chrome(options=o)
wd.close()
'
