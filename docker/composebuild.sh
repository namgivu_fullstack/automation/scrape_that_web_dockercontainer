nocache=${nocache:-''}  # set nocache=--no-cache to force build image without cache
#
SH=`cd $(dirname $BASH_SOURCE) && pwd`
GH="$SH/.."  # GH aka GIT_CLONED_HOME
cd $SH
    GH=$GH docker-compose build $nocache
